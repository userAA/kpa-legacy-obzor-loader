﻿let global = {
	cmdEP: null,
	isConnectionErrorPopupsEnabled: true,
	areHttpRequestsEnabled: true,
	isTopMenuEnabled: true,
	wasConnectionError: false,

	callbacks: {
		onserverreload: null
	},

	bottomMenuUpdateInterval: 200,
	telemUpdateInterval: 300,
	isPageChanged: false,
	isPowerEnabled: false
};

function getElemsWithIDRecursive(rootNode, result)
{
	for (let i = 0; i < rootNode.childElementCount; ++i) {
		let elem = rootNode.children[i];

		if (elem.id.length != 0) {
			result[elem.id] = elem;
		}

		if (elem.childElementCount != 0) {
			getElemsWithIDRecursive(elem, result);
		}
	}
}


function getElemsWithID(rootNode = null)
{
	let result = {};

	if (rootNode === null) {
		rootNode = document.body;
	}

	getElemsWithIDRecursive(rootNode, result);

	return result;
}


function sendHttpGetRequest(uri, onResponse, onConnectError = null)
{
	sendHttpRequest(uri, "GET", null, onResponse, generateIntID().toString(), null, onConnectError);
}


function sendHttpPostRequest(uri, data, onResponse, additionalHeaders = null, onConnectError = null, onProgress = null)
{
	sendHttpRequest(uri, "POST", data, onResponse, generateIntID().toString(), additionalHeaders, onConnectError, onProgress);
}

var xhrConnectErrorPrinted = {};

function sendHttpRequest(uri, method, data, onResponse, requestID, additionalHeaders = null, onConnectError = null, onProgress = null)
{
	let onXhrError = () =>
	{
		//if (global.isConnectionErrorPopupsEnabled) {
			if (!xhrConnectErrorPrinted[requestID]) {
				//Popup.showLocked("Ошибка сервера.", "Сервер выключен или не отвечает.");
				xhrConnectErrorPrinted[requestID] = true;
			}
		//}

		//global.wasConnectionError = true;

		if (onConnectError) {
			onConnectError();
		}

		setTimeout(() =>
		{ 
			sendHttpRequest(uri, method, data, onResponse, requestID, additionalHeaders, null, onProgress); 
		}, 1000);
	}

	let xhr = new XMLHttpRequest();

	xhr.timeout = 30000;
	xhr.open(method, uri, true);
	xhr.setRequestHeader("Cache-Control", "no-cache");

	if (additionalHeaders) {
		for (let i = 0; i < additionalHeaders.length; ++i) {
			let header = additionalHeaders[i];
			xhr.setRequestHeader(header.name, header.value);
		}
	}
	
	xhr.onerror = onXhrError;
	xhr.ontimeout = onXhrError;
	xhr.upload.onprogress = onProgress;

	xhr.onload = () =>
	{
		if (global.wasConnectionError) {
			global.wasConnectionError = false;
			//onServerConnectionRestored();
		}

		let response = xhr.response;

		if (xhrConnectErrorPrinted[requestID]) {
			delete xhrConnectErrorPrinted[requestID];
			//Popup.close();
		}
		

		if (onResponse) {
			onResponse(response);
		}
	}

	xhr.send(data);
}


function generateIntID()
{
	return Number(Math.floor((Math.random() / 2.0) * 0xFFFFFFFF));
}


function generateStringID()
{
	return generateIntID().toString();
}