﻿class Timer {
	constructor()
	{
		this.secs = 0;
		this.onTick = null;
		this.onDeadline = null;
		this.toStop = false;
	}

	stop()
	{
		console.log("таймер остановлен");
		this.toStop = true;

	}

	start(secs, tickLenght, onTick, onDeadline)
	{
		this.tickLenght = tickLenght;
		this.secs = secs;
		this.onTick = onTick;
		this.onDeadline = onDeadline;
		this.toStop = false;
		this._onTick();
	}

	_onTick()
	{
		if (this.toStop)
			return;

		this.onTick(this.secs);

		if (this.secs >= 0) {
			setTimeout(() => {
				this.secs -= this.tickLenght / 1000;
				this._onTick.call(this, this.secs, this.onTick, this.onDeadline, this.toStop);
			}, this.tickLenght);
		} else {
			this.onDeadline();
		}
	}
};


function main()
{
	const secsToWait = 35;
	const elems = getElemsWithID();
	const timer = new Timer();

	function onTimerEnd()
	{
		timer.stop();
		window.location.href = "redirect.html";
	}

	//elems.labelTimer.textContent = secsToWait.toString();
	//elems.buttonSkipTimer.onclick = onTimerEnd;


	function stopTimer()
	{
		timer.stop();
		elems.progressLoading.hidden = true;
		elems.trRedirectionWarning.hidden = true;
		//elems.buttonStopTimer.disabled = true;
		//elems.trRedirectionWarning.hidden = true;
	}


	//elems.buttonStopTimer.onclick = stopTimer;
	timer.start(secsToWait, 50, (v) =>
	{
		elems.progressLoading.value = (secsToWait - v) / secsToWait * elems.progressLoading.max;
		//elems.labelTimer.textContent = v.toString();
	}, onTimerEnd);
	

	elems.fileUpload.onclick = stopTimer;
	elems.fileUpload.onchange = (e) =>
	{
		elems.buttonUpload.disabled = false;
	};


	sendHttpGetRequest("__get_ep", (response) =>
	{
		const j = JSON.parse(response);
		const hostname = j[1];
		const ipConfig = j[0];

		elems.labelHostname.textContent = hostname;
		elems.labelIP.textContent = `${ipConfig.ip}/${ipConfig.subnetMask}`;
	});
}