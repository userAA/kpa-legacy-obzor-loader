﻿if (process.argv.length < 3) {
	console.log("Загрзчик КПА. Параметры командной строки:");
	console.log("\t--kpa_path=string | Путь к софту КПА");
	console.log("Если в папке с софтом КПА будет находиться файл \"autorun.cmd\", то он будет запускаться после перенаправления на страницу с КПА");
	console.log("Если в папке с софтом КПА будет находиться файл \"run_once.cmd\", то он будет запускаться только один раз после обновления");
	return;
}

const http = require("http");
const fs = require("fs");
const formidable 			= require("formidable");
const os 					= require("os");
const { 
	getIPConfig, 
	setIPConfig, 
	IPConfig } 				= require("./changeIP");
const StreamZip 			= require("node-stream-zip");
const { exec, spawn } 		= require("child_process");
const path 					= require("path");
const frontentRootPath 		= "frontend";
const KPASoftRootPath 		= process.argv[2];
const KPASoftRootPathAbs 	= path.resolve(KPASoftRootPath);
const KPARunOnceFilePath 	= `${KPASoftRootPathAbs}\\run_once.cmd`;
const KPAAutorunFilePath 	= `${KPASoftRootPathAbs}\\autorun.cmd`;

let toRunAfter = [];


http.createServer(async function (req, res) {
	if (req.method == "POST" && req.url == "/upload") {
		const form = new formidable.IncomingForm();

		form.parse(req, function (err, fields, files) {
            // oldpath : temporary folder to which file is saved to
            let oldpath = files.filetoupload.path;
            const uploadPath = `${Math.random()}_${files.filetoupload.name}`;
            // copy the file to a new location
            fs.rename(oldpath, uploadPath, async function (err) {
				if (err) return;

				try {
					const zip = new StreamZip.async({ file: uploadPath });
					
					if (fs.existsSync(KPASoftRootPathAbs)) {
						fs.rmdirSync(KPASoftRootPathAbs, { recursive: true, force: true });
					}
	
					fs.mkdirSync(KPASoftRootPathAbs);
					await zip.extract(null, KPASoftRootPathAbs);
					await zip.close();
					fs.unlink(uploadPath, () => {});

					if (fs.existsSync(KPARunOnceFilePath)) {
						const cmd = spawn("cmd.exe", ["/c", KPARunOnceFilePath], { shell: true, cwd: KPASoftRootPathAbs });
						cmd.on("error", console.log);
					}
				} catch (error) {}

				res.writeHead(301, { "Location": "redirect.html" });
				return res.end();
            });
        });
	} else if (req.method == "GET") {
		const fileName = req.url === "/" ? "/index.html" : req.url;
		const fullFileName = `${frontentRootPath}${fileName}`;

		res.setHeader("Access-Control-Allow-Origin", "*");
		res.setHeader("Access-Control-Allow-Methods", "*");
		res.setHeader("Access-Control-Allow-Headers", "*");

		if (req.url === "/__get_ep") {
			res.setHeader("Content-Type", "application/json");
			res.writeHead(200);
			
			try {
				res.write(JSON.stringify([await getIPConfig(), os.hostname()]));
			} catch (error) {
				res.write(JSON.stringify(error));
			}
		} else if (fs.existsSync(fullFileName)) {
			if (req.url === "/redirect.html") {
				if (fs.existsSync(KPAAutorunFilePath)) {
					const cmd = spawn("cmd.exe", ["/c", KPAAutorunFilePath], { shell: true, cwd: KPASoftRootPathAbs });
					cmd.on("error", console.log);
				}
			}

			if (fullFileName.endsWith(".css")) {
				res.setHeader("Content-Type", "text/css");
			} else if (fullFileName.endsWith(".js")) {
				res.setHeader("Content-Type", "text/javascript");
			} else if (fullFileName.endsWith(".html")) {
				res.setHeader("Content-Type", "text/html");
			} else if (fullFileName.endsWith(".ttf")) {
				res.setHeader("Content-Type", "application/x-font-ttf");
			} else {
				res.setHeader("Content-Type", "text/plain");
			}
			
			res.writeHead(200);
			res.write(fs.readFileSync(fullFileName));
		} else {
			res.writeHead(404);
		}

		return res.end();
	} else if (req.method === "OPTIONS") {
		res.setHeader("Access-Control-Allow-Origin", "*");
		res.setHeader("Access-Control-Allow-Methods", "*");
		res.setHeader("Access-Control-Allow-Headers", "*");

		res.writeHead(200);
		return res.end();
	} else if (req.method === "POST") {
		let buffer = "";

		if (req.url === "/__set_ep") {
			req.on("data", (d) => { buffer += d; });
			req.on("end", async () =>
			{
				try {
					//toRunAfter.push(async () => 
					//{
					//});
					
					const j = JSON.parse(buffer);
					await setIPConfig(j);
					res.setHeader("Access-Control-Allow-Origin", "*");
					res.setHeader("Access-Control-Allow-Methods", "*");
					res.setHeader("Access-Control-Allow-Headers", "*");
					res.writeHead(200);
					res.end();
					console.log("res end");
				} catch (error) {
					res.setHeader("Access-Control-Allow-Origin", "*");
					res.setHeader("Access-Control-Allow-Methods", "*");
					res.setHeader("Access-Control-Allow-Headers", "*");
					res.writeHead(200);
					res.write(JSON.stringify(error));
					res.end();
				}
			});
		}
	}
}).listen(8999);