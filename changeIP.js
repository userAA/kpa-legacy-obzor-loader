﻿const { exec } 		= require("child_process");
const os 			= require("os");

class IPConfig {
	constructor(ip = "127.0.0.1", subnetMask = "1", isDHCPEnabled = false, gateway = "0.0.0.0", name = "")
	{
		this.ip = ip;
		this.subnetMask = subnetMask;
		this.isDHCPEnabled = isDHCPEnabled;
		this.gateway = gateway;
		this.name = name;
	}
}

function getIPConfig()
{
	return new Promise((resolve, reject) =>
	{
		const result = new IPConfig();
		
		exec("chcp 65001");
		exec("netsh interface ipv4 show config", { shell: true }, (error, stdout, stderr) =>
		{
			if (error) {
				reject(error);
				return;
			}
		//	stdout = "\r\n" + `Configuration for interface "Ethernet 2"
		//	DHCP enabled:                         No
		//	Default Gateway:                      192.168.1.1
		//	Gateway Metric:                       1
		//	InterfaceMetric:                      35
		//	Statically Configured DNS Servers:    None
		//	Register with which suffix:           Primary only
		//	Statically Configured WINS Servers:   None
		//
		//Configuration for interface "Loopback Pseudo-Interface 1"
		//	DHCP enabled:                         No
		//	IP Address:                           127.0.0.1
		//	Subnet Prefix:                        127.0.0.0/8 (mask 255.0.0.0)
		//	InterfaceMetric:                      75
		//	Statically Configured DNS Servers:    None
		//	Register with which suffix:           None
		//	Statically Configured WINS Servers:   None`;
			
			let interfaces = stdout.toString().split("Configuration for interface ");

			console.log(stdout.toString());

			interfaces.forEach((v, i, arr) => { if (v === "\r\n") arr.splice(i, 1);  });

			for (let i = 0; i < interfaces.length; i++) {
				const v = interfaces[i];
				const name = v.substring(1, v.indexOf("\"", 1));

				const ipAddressIndex = v.indexOf("IP Address");

				if (ipAddressIndex === -1)
					continue;

				let ipResultIndex = ipAddressIndex;

				for (; ipResultIndex < v.length; ipResultIndex++) {
					const char = v[ipResultIndex];
					
					if (char >= "0" && char <= "9") {
						break;
					}
				}
				const ipResult = v.substring(ipResultIndex, v.indexOf("\r\n", ipResultIndex));

				if (ipResult === "127.0.0.1")
					continue;

				result.ip = ipResult;
				
				const dhcpStringIndex = v.indexOf("DHCP enabled");
				let dhcpResultIndex = v.indexOf(":", dhcpStringIndex);

				for (; dhcpResultIndex < v.length; dhcpResultIndex++) {
					const char = v[dhcpResultIndex];
					
					if (char === "Y" || char === "N") {
						break;
					}
				}

				const dhcpResult = v.substring(dhcpResultIndex, v.indexOf("\r\n", dhcpResultIndex));
				result.isDHCPEnabled = dhcpResult === "Yes";

				let subnetMaskIndex = v.indexOf("Subnet Prefix");
				
				if (subnetMaskIndex !== -1) {
					let subnetMaskResultIndex = v.indexOf("/", subnetMaskIndex) + 1;

					const subnetMaskResult = v.substring(subnetMaskResultIndex, v.indexOf(" ", subnetMaskResultIndex));
					result.subnetMask = parseInt(subnetMaskResult);
				}

				result.name = name;

				const gatewayIndex = v.indexOf("Default Gateway");

				if (gatewayIndex !== -1) {
					let gatewayResultIndex = gatewayIndex;
					
					for (; gatewayResultIndex < v.length; gatewayResultIndex++) {
						const char = v[gatewayResultIndex];

						if (char >= "0" && char <= "9") {
							break;
						}
					}
					
					const gatewayResult = v.substring(gatewayResultIndex, v.indexOf("\r\n", gatewayResultIndex));
					result.gateway = gatewayResult;
				}
		
				break;
			}

			resolve(result);
		});
	});
}


function setIPConfig(ipConfig)
{
	return new Promise(async (resolve, reject) =>
	{
		if (ipConfig.name === "" || !ipConfig.name) {
			const networkInterfaces = os.networkInterfaces();
			
			for (const key in networkInterfaces) {
				ipConfig.name = key;
				break;
			}
		}

		exec("chcp 65001");
		const commandLine = `netsh interface ipv4 set address name="${ipConfig.name}" ${ipConfig.isDHCPEnabled ? "dhcp" : `static ${ipConfig.ip}/${ipConfig.subnetMask} gateway=${ipConfig.gateway}`}`;
		console.log(commandLine);
		exec(commandLine, { shell: true }, (error, stdout, stderr) => 
		{
			if (error)
				console.error(error);
			
			resolve(stdout);
		});
	});
}

module.exports = { getIPConfig, setIPConfig, IPConfig };